import { MenuItemType } from '@paljs/ui/types';

export const adminItems: MenuItemType[] = [
    {
        title: 'Start',
        icon: { name: 'keypad-outline' },
        link: { href: '/landing' },
    },
    {
        title: 'Verwaltung',
        icon: { name: 'layers-outline' },
        link: { href: '/admin' },
    },
    {
        title: 'Konfiguration',
        icon: { name: 'settings-outline' },
        link: { href: '/config' },
    },
    {
        title: 'Blog',
        icon: { name: 'archive-outline' },
        link: { href: '/config' },
    },
    {
        title: 'playUniverse Communities',
        group: true,
    },
];

const items: MenuItemType[] = [
    {
        title: 'Start',
        icon: { name: 'keypad-outline' },
        link: { href: '/landing' },
    },
    {
        title: 'Dashboard',
        icon: { name: 'home' },
        link: { href: '/dashboard' },
    },
    {
        title: 'Blog',
        icon: { name: 'archive-outline' },
        link: { href: '/config' },
    },
    {
        title: 'playUniverse Communities',
        group: true,
    },

    // {
    //   title: 'About',
    //   icon: { name: 'keypad-outline' },
    //   children: [
    //     {
    //       title: 'Login',
    //       link: { href: '/auth/login' },
    //     },
    //     {
    //       title: 'Register',
    //       link: { href: '/auth/register' },
    //     },
    //     {
    //       title: 'Request Password',
    //       link: { href: '/auth/request-password' },
    //     },
    //     {
    //       title: 'Reset Password',
    //       link: { href: '/auth/reset-password' },
    //     },
    //   ],
    // },
];

export default items;
