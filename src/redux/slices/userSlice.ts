// @ts-nocheck

import { createSlice, Draft, PayloadAction } from '@reduxjs/toolkit';
import { AppDispatch } from 'redux/store';
import { toast } from 'react-toastify';

export interface Request {
    id: string;
    createdAt: string;
    updatedAt: string;
    status: number;
    slots: number;
    memory: number;
    user: string;
    community: string;
    queue: number;
    serverIp: string;
    serverPort: number;
    serverId: number;
    serverFriendlyId: string;
    serverHost: string;
}

export interface Config {
    minMem: number;
    maxMem: number;
    minSlots: number;
    maxSlots: number;
    formEnabled: boolean;
}

export interface Community {
    id: string;
    createdAt: string;
    updatedAt: string;
    user: number;
    homepage: string;
    name: string;
    description: string;
    discord: string;
    email: string;
}

interface UserState {
    id: number;
    access: string;
    firstName: string;
    lastName: string;
    email: string;
    isStaff: boolean;
    adminRequests: Request[];
    userRequests: Request[];
    communities: Community[];
    eggs: Egg[];
    userLoading: boolean;
    loginLoading: boolean;
    registerLoading: boolean;
    registerSuccess: boolean;
    activateLoading: boolean;
    activateSuccess: boolean;
    updateProfileLoading: boolean;
    adminRequestsLoading: boolean;
    userRequestsLoading: boolean;
    communityLoading: boolean;
    config: Config;
    configLoading: boolean;
    requestChangeLoading: boolean;
    eggsLoading: boolean;
    createRequestLoading: boolean;
}

export type Egg = {
    name: string;
    id: number;
    uuid: string;
    category: number;
};

const initialState: UserState = {
    id: 0,
    access: '',
    firstName: '',
    lastName: '',
    email: '',
    isStaff: false,
    adminRequests: new Array(),
    userRequests: new Array(),
    communities: new Array(),
    eggs: new Array(),
    userLoading: false,
    loginLoading: false,
    registerLoading: false,
    registerSuccess: false,
    activateLoading: false,
    activateSuccess: false,
    updateProfileLoading: false,
    adminRequestsLoading: false,
    userRequestsLoading: false,
    communityLoading: false,
    config: {
        minMem: 0,
        maxMem: 0,
        minSlots: 0,
        maxSlots: 0,
        formEnabled: false,
    },
    configLoading: false,
    requestChangeLoading: false,
    eggsLoading: false,
    createRequestLoading: false,
};

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        resetUser: (state: Draft<typeof initialState>) => {
            state.access = '';

            localStorage.clear();
            toast.success('Signed out!');
        },
        loginSuccess: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState>) => {
            state.access = action.payload.access;
            toast.success('Login successfull!');
            state.loginLoading = false;
            localStorage.setItem('jwt', action.payload.access);
        },
        loginFail: (state: Draft<typeof initialState>) => {
            toast.error('Data incorrect!');
            state.loginLoading = false;
        },
        loginLoading: (state: Draft<typeof initialState>) => {
            state.loginLoading = true;
        },
        fetchUserLoading: (state: Draft<typeof initialState>) => {
            state.userLoading = true;
        },
        fetchUserSuccess: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState>) => {
            state.firstName = action.payload.firstName;
            state.lastName = action.payload.lastName;
            state.email = action.payload.email;
            state.isStaff = action.payload.isStaff;
            state.userLoading = false;
        },
        fetchUserFail: (state: Draft<typeof initialState>) => {
            state.userLoading = false;
        },
        registerLoading: (state: Draft<typeof initialState>) => {
            state.registerLoading = true;
        },
        registerSuccess: (state: Draft<typeof initialState>) => {
            state.registerLoading = false;
            state.registerSuccess = true;
            state.activateSuccess = false;
            toast.success('Erfolgreich! Bitte checke deine Mails.');
        },
        registerFail: (state: Draft<typeof initialState>) => {
            state.registerLoading = false;
            toast.error('Fehler! Etwas hat nicht funktioniert.');
        },
        activateLoading: (state: Draft<typeof initialState>) => {
            state.activateLoading = true;
        },
        activateSuccess: (state: Draft<typeof initialState>) => {
            state.activateLoading = false;
            toast.success('Account erfolgreich aktiviert.');
        },
        activateFail: (state: Draft<typeof initialState>) => {
            state.activateLoading = false;
            toast.error('Fehler! Etwas hat nicht funktioniert.');
        },
        updateProfileLoading: (state: Draft<typeof initialState>) => {
            state.updateProfileLoading = true;
        },
        updateProfileSuccess: (state: Draft<typeof initialState>) => {
            state.updateProfileLoading = false;
            toast.success('Daten erfolgreich aktualisiert.');
        },
        updateProfileFail: (state: Draft<typeof initialState>) => {
            state.updateProfileLoading = false;
        },
        fetchAdminRequestsLoading: (state: Draft<typeof initialState>) => {
            state.adminRequestsLoading = true;
            state.requestChangeLoading = false;
        },
        fetchAdminRequestsSuccess: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState>) => {
            state.adminRequestsLoading = false;
            state.adminRequests = action.payload;
        },
        fetchAdminRequestsFail: (state: Draft<typeof initialState>) => {
            state.adminRequestsLoading = true;
        },
        fetchCommunityLoading: (state: Draft<typeof initialState>) => {
            state.communityLoading = true;
        },
        fetchCommunitySuccess: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState>) => {
            state.communityLoading = false;
            const index = state.communities.indexOf((c) => c.id == action.payload.id);
            if (index >= 0) {
                state.communities[index] = action.payload;
            } else {
                state.communities.push(action.payload);
            }
        },
        fetchCommunityFail: (state: Draft<typeof initialState>) => {
            state.communityLoading = false;
        },
        fetchConfigLoading: (state: Draft<typeof initialState>) => {
            state.configLoading = true;
        },
        fetchConfigSuccess: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState>) => {
            state.configLoading = false;
            state.config = action.payload;
        },
        fetchConfigFail: (state: Draft<typeof initialState>) => {
            state.configLoading = false;
        },
        requestChangeLoading: (state: Draft<typeof initialState>) => {
            state.requestChangeLoading = true;
        },
        requestChangeSuccess: (state: Draft<typeof initialState>) => {
            state.requestChangeLoading = false;
            toast.success('Anfrage aktualisiert.');
        },
        requestChangeFail: (state: Draft<typeof initialState>) => {
            state.requestChangeLoading = false;
        },
        fetchEggsLoading: (state: Draft<typeof initialState>) => {
            state.eggsLoading = true;
        },
        fetchEggsSuccess: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState>) => {
            state.eggsLoading = false;
            state.eggs = action.payload;
        },
        fetchEggsFail: (state: Draft<typeof initialState>) => {
            state.eggsLoading = false;
        },
        createRequestLoading: (state: Draft<typeof initialState>) => {
            state.createRequestLoading = true;
        },
        createRequestSuccess: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState>) => {
            state.createRequestLoading = false;
        },
        createRequestFail: (state: Draft<typeof initialState>) => {
            state.createRequestLoading = false;
        },
        fetchUserRequestsLoading: (state: Draft<typeof initialState>) => {
            state.userRequestsLoading = true;
        },
        fetchUserRequestsSuccess: (state: Draft<typeof initialState>, action: PayloadAction<typeof initialState>) => {
            state.userRequestsLoading = false;
            state.userRequests = action.payload;
        },
        fetchUserRequestsFail: (state: Draft<typeof initialState>) => {
            state.userRequestsLoading = false;
        },
        deleteRequestLoading: (state: Draft<typeof initialState>) => {
            state.requestChangeLoading = true;
        },
        deleteRequestSuccess: (state: Draft<typeof initialState>) => {
            state.requestChangeLoading = false;
        },
        deleteRequestFail: (state: Draft<typeof initialState>) => {
            state.requestChangeLoading = false;
        },
        changeConfigLoading: (state: Draft<typeof initialState>) => {
            state.configLoading = true;
        },
        changeConfigSuccess: (state: Draft<typeof initialState>) => {
            state.configLoading = false;
            toast.success('Daten erfolgreich aktualisiert.');
        },
        changeConfigFail: (state: Draft<typeof initialState>) => {
            state.configLoading = false;
        },
        deleteUserLoading: (state: Draft<typeof initialState>) => {
            state.userLoading = true;
        },
        deleteUserSuccess: (state: Draft<typeof initialState>) => {
            state.userLoading = false;
        },
        deleteUserFail: (state: Draft<typeof initialState>) => {
            state.userLoading = false;
        },
    },
});

export const login = (email: string, password: string) => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/auth/jwt/create',
                method: 'POST',
                data: { username: email, password },
                auth: false,
                onSuccess: loginSuccess.type,
                onSuccessAction: fetchUser,
                onError: loginFail.type,
                onLoading: loginLoading.type,
            },
        });
    };
};

export const register = (email: string, password: string) => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/auth/users/',
                method: 'POST',
                data: { username: email, email: email, password },
                auth: false,
                onSuccess: registerSuccess.type,
                onError: registerFail.type,
                onLoading: registerLoading.type,
            },
        });
    };
};

export const fetchUser = () => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/users/me/',
                method: 'GET',
                auth: true,
                onSuccess: fetchUserSuccess.type,
                onLoading: fetchUserLoading.type,
                onError: fetchUserFail.type,
            },
        });
    };
};

export const activate = (uid: String, token: String) => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/auth/users/activation/',
                method: 'POST',
                data: { uid, token },
                auth: false,
                onSuccess: activateSuccess.type,
                onLoading: activateLoading.type,
                onError: activateFail.type,
            },
        });
    };
};

export const updateProfile = (firstName: string, lastName: string) => {
    return (dispatch: AppDispatch) => {
        const fd = new FormData();
        if (firstName) fd.append('first_name', firstName);
        if (lastName) fd.append('last_name', lastName);
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/profiles/your-profile/',
                method: 'PATCH',
                data: fd,
                auth: true,
                onSuccess: updateProfileSuccess.type,
                onLoading: updateProfileLoading.type,
                onError: updateProfileFail.type,
                onSuccessAction: fetchUser,
            },
        });
    };
};

export const fetchRequests = () => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/requests/',
                method: 'GET',
                auth: true,
                onSuccess: fetchAdminRequestsSuccess.type,
                onLoading: fetchAdminRequestsLoading.type,
                onError: fetchAdminRequestsFail.type,
                onSuccessListFetch: { action: fetchCommunity, id: 'community' },
            },
        });
    };
};

export const fetchCommunity = (community: string) => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/communities/' + community + '/',
                method: 'GET',
                auth: true,
                onSuccess: fetchCommunitySuccess.type,
                onLoading: fetchCommunityLoading.type,
                onError: fetchCommunityFail.type,
            },
        });
    };
};

export const fetchConfig = () => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/config/1/',
                method: 'GET',
                onSuccess: fetchConfigSuccess.type,
                onLoading: fetchConfigLoading.type,
                onError: fetchConfigFail.type,
            },
        });
    };
};

export const changeRequestStatus = (request: string, status: number, installation?: string) => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/requests/' + request + '/',
                method: 'PATCH',
                data: { status, installation },
                auth: true,
                onSuccess: requestChangeSuccess.type,
                onLoading: requestChangeLoading.type,
                onError: requestChangeFail.type,
                onSuccessAction: fetchRequests,
            },
        });
    };
};

export const fetchEggs = () => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/fennek?action=get_eggs_min',
                method: 'GET',
                onSuccess: fetchEggsSuccess.type,
                onLoading: fetchEggsLoading.type,
                onError: fetchEggsFail.type,
            },
        });
    };
};

export const createRequest = (
    slots: any,
    mem: any,
    firstName: any,
    lastName: any,
    email: any,
    password: string,
    communityName: any,
    homepage: any,
    description: any,
) => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/create-request',
                method: 'POST',
                data: { slots, mem, firstName, lastName, email, password, communityName, homepage, description },
                onSuccess: fetchEggsSuccess.type,
                onLoading: fetchEggsLoading.type,
                onError: fetchEggsFail.type,
                onSuccessAction: fetchRequests,
            },
        });
    };
};

export const fetchUserRequests = () => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/requests/',
                method: 'GET',
                auth: true,
                onSuccess: fetchUserRequestsSuccess.type,
                onLoading: fetchUserRequestsLoading.type,
                onError: fetchUserRequestsFail.type,
                onSuccessListFetch: { action: fetchCommunity, id: 'community' },
            },
        });
    };
};

export const deleteRequest = (id: string) => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/requests/' + id,
                method: 'DELETE',
                auth: true,
                onSuccess: deleteRequestSuccess.type,
                onLoading: deleteRequestLoading.type,
                onError: deleteRequestFail.type,
                onSuccessAction: fetchRequests,
            },
        });
    };
};

export const updateConfig = (
    minMem: number,
    maxMem: number,
    minSlots: number,
    maxSlots: number,
    formEnabled: boolean,
) => {
    return (dispatch: AppDispatch) => {
        let data = {};
        if (minMem > 0) data['minMem'] = minMem;
        if (maxMem > 0) data['maxMem'] = maxMem;
        if (minSlots > 0) data['minSlots'] = minSlots;
        if (maxSlots > 0) data['maxSlots'] = maxSlots;
        data['formEnabled'] = formEnabled;

        dispatch({
            type: 'apiCall',
            payload: {
                url: '/config/1/',
                method: 'PATCH',
                auth: true,
                data: data,
                onSuccess: changeConfigSuccess.type,
                onLoading: changeConfigLoading.type,
                onError: changeConfigFail.type,
                onSuccessAction: fetchConfig,
            },
        });
    };
};

export const deleteUser = () => {
    return (dispatch: AppDispatch) => {
        dispatch({
            type: 'apiCall',
            payload: {
                url: '/auth/users/me/',
                method: 'DELETE',
                auth: true,
                onSuccess: deleteUserSuccess.type,
                onLoading: deleteUserLoading.type,
                onError: deleteUserFail.type,
            },
        });
    };
};

// Selectors
export const getUser = (state: { user: any }) => state.user;

export const getCommunity = (communities: Community[], community: string) =>
    communities.filter((com) => com.id === community)[0];

// Reducers and actions
export const {
    resetUser,
    loginSuccess,
    loginFail,
    loginLoading,
    fetchUserLoading,
    fetchUserSuccess,
    fetchUserFail,
    registerLoading,
    registerSuccess,
    registerFail,
    activateLoading,
    activateSuccess,
    activateFail,
    updateProfileLoading,
    updateProfileSuccess,
    updateProfileFail,
    fetchAdminRequestsLoading,
    fetchAdminRequestsSuccess,
    fetchAdminRequestsFail,
    fetchCommunityLoading,
    fetchCommunitySuccess,
    fetchCommunityFail,
    fetchConfigLoading,
    fetchConfigSuccess,
    fetchConfigFail,
    requestChangeLoading,
    requestChangeSuccess,
    requestChangeFail,
    fetchEggsLoading,
    fetchEggsSuccess,
    fetchEggsFail,
    createRequestLoading,
    createRequestSuccess,
    createRequestFail,
    fetchUserRequestsLoading,
    fetchUserRequestsSuccess,
    fetchUserRequestsFail,
    deleteRequestLoading,
    deleteRequestSuccess,
    deleteRequestFail,
    changeConfigLoading,
    changeConfigSuccess,
    changeConfigFail,
    deleteUserLoading,
    deleteUserSuccess,
    deleteUserFail,
} = userSlice.actions;

export default userSlice.reducer;
