import { Action, configureStore, Middleware, ThunkAction } from '@reduxjs/toolkit';
import api from './middleware';

import userReducer from './slices/userSlice';

export const store: any = configureStore({
  reducer: {
    user: userReducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().prepend(
      // correctly typed middlewares can just be used
      api as Middleware<(action: Action<'specialAction'>) => number, RootState>,
    ),
});

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
