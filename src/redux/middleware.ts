import axios from 'axios';

const api =
    ({ dispatch }: any) =>
    (next: (arg0: any) => any) =>
    async (action: {
        type: string;
        payload: {
            auth?: any;
            url?: any;
            method?: any;
            data?: any;
            onSuccess?: any;
            onError?: any;
            onLoading?: any;
            target?: any;
            onSuccessAction?: any;
            onSuccessListFetch?: any;
        };
    }) => {
        if (action.type !== 'apiCall') return next(action);
        const headers: any = { 'Content-Type': 'application/json' };
        if (action.payload.auth) headers.Authorization = 'JWT ' + localStorage.getItem('jwt');
        const { url, method, data, onSuccess, onError, onLoading, target, onSuccessAction, onSuccessListFetch } =
            action.payload;
        if (onLoading) dispatch({ type: onLoading });
        try {
            const response = await axios.request({
                baseURL: process.env.NEXT_PUBLIC_API,
                headers,
                url,
                data,
                method,
            });
            dispatch({ type: onSuccess, payload: response.data, target });
            if (onSuccessAction) dispatch(onSuccessAction());
            if (onSuccessListFetch) {
                response.data.map((element: any) => {
                    dispatch(onSuccessListFetch.action(element[onSuccessListFetch.id]));
                });
            }
        } catch (error) {
            if (onError) {
                dispatch({ type: onError, payload: error });
            }
        }
    };

export default api;
