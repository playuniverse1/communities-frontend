import { Provider } from 'react-redux';
import type { AppProps } from 'next/app';
import { store } from 'redux/store';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { fetchConfig, fetchUser } from 'redux/slices/userSlice';

export default function app({ Component, pageProps }: AppProps) {
    if (typeof window !== 'undefined' && localStorage.getItem('jwt')) {
        store.dispatch(fetchUser());
    }
    store.dispatch(fetchConfig());
    return (
        <Provider store={store}>
            <Component {...pageProps} />
            <ToastContainer />
        </Provider>
    );
}
