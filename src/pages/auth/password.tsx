import { Button } from '@paljs/ui/Button';
import React from 'react';

import Auth from 'components/Auth';
import Layout from 'Layouts';
import { AppDispatch } from 'redux/store';
import { activate } from 'redux/slices/userSlice';
import { connect } from 'react-redux';
import Router from 'next/router';
import { Spinner } from '@paljs/ui';

class Password extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
    }
    static getInitialProps({ query }: any) {
        return { query };
    }

    componentDidMount() {
        const { uid, token } = this.props.query;
        this.props.activate(uid, token);
    }

    render() {
        if (this.props.activateSuccess) {
            Router.push('/auth/password');
        }

        return (
            <Layout title="Aktiviere deinen Account">
                <Auth title="Aktiviere deinen Account" subTitle="Wir aktivieren deinen Account">
                    <form>
                        <Button
                            status="Success"
                            type="button"
                            shape="SemiRound"
                            disabled
                            fullWidth
                            style={{ position: 'relative' }}
                        >
                            Ativierenl {this.props.activateLoading && <Spinner status="Primary" />}
                        </Button>
                    </form>
                </Auth>
            </Layout>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        activateSuccess: state.user.activateSuccess,
        activateLoading: state.user.activateLoading,
    };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        setPassword: (uid: string, token: string) => dispatch(activate(uid, token)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Password);
