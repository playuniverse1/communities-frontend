import React from 'react';

import { withRouter } from 'next/router';
import { connect } from 'react-redux';
import { AppDispatch } from 'redux/store';
import { resetUser } from 'redux/slices/userSlice';
import Layout from 'Layouts';
import Auth from 'components/Auth';

class Logout extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.props.logout();
        this.props.router.push('/auth/login');
    }
    render() {
        return (
            <Layout title="Logout">
                <Auth title="Wir melden dich ab" subTitle="Das sollte nicht so lange dauern." />
            </Layout>
        );
    }
}

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        logout: () => dispatch(resetUser()),
    };
};

export default withRouter(connect(null, mapDispatchToProps)(Logout));
