import { Button } from '@paljs/ui/Button';
import React from 'react';

import Auth from 'components/Auth';
import Layout from 'Layouts';
import { withRouter } from 'next/router';

class AccountConfirm extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }

  render() {
    return (
      <Layout title="Confirm your e-mail address">
        <Auth title="Confirm your e-mail address" subTitle="Please check your inbox.">
          <form>
            <Button
              status="Primary"
              type="button"
              shape="SemiRound"
              fullWidth
              onClick={() => this.props.router.push('/auth/register')}
            >
              Go back
            </Button>
          </form>
        </Auth>
      </Layout>
    );
  }
}

export default withRouter(AccountConfirm);
