import { Button } from '@paljs/ui/Button';
import { InputGroup } from '@paljs/ui/Input';
import React from 'react';
import Link from 'next/link';

import Auth, { Group } from 'components/Auth';
import Layout from 'Layouts';
import { AppDispatch } from 'redux/store';
import { login } from 'redux/slices/userSlice';
import { connect } from 'react-redux';
import Router from 'next/router';
import { Spinner } from '@paljs/ui';

class Login extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            email: '',
            password: '',
        };
    }

    handleChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleLogin = (e: any) => {
        e.preventDefault();
        this.props.login(this.state.email, this.state.password);
    };

    render() {
        if (this.props.authenticated && typeof window !== 'undefined') {
            Router.push('/dashboard');
        }

        const { email, password } = this.state;
        return (
            <Layout title="Anmelden">
                <Auth title="Anmelden" subTitle="Melde dich mit deiner E-Mail an.">
                    <form action="" onSubmit={this.handleLogin}>
                        <InputGroup fullWidth>
                            <input
                                type="email"
                                name="email"
                                placeholder="Email"
                                onChange={this.handleChange}
                                value={email}
                            />
                        </InputGroup>
                        <InputGroup fullWidth>
                            <input
                                type="password"
                                name="password"
                                placeholder="Passwort"
                                onChange={this.handleChange}
                                value={password}
                            />
                        </InputGroup>
                        <Group>
                            <Link href="/auth/request-password">
                                <a>Passwort vergessen?</a>
                            </Link>
                        </Group>
                        <Button
                            status="Success"
                            type="submit"
                            shape="SemiRound"
                            fullWidth
                            onClick={this.handleLogin}
                            style={{ position: 'relative' }}
                            disabled={!email || !password || this.props.loginLoading}
                        >
                            Anmelden
                            {this.props.loginLoading && <Spinner status="Primary" />}
                        </Button>
                    </form>
                    <p>
                        Keinen Account?{' '}
                        <Link href="/">
                            <a>Anfrage stellen</a>
                        </Link>
                    </p>
                </Auth>
            </Layout>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        authenticated: state.user.access,
        loginLoading: state.user.loginLoading,
    };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        login: (email: string, password: string) => dispatch(login(email, password)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
