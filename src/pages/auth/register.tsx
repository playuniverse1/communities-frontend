import { Button } from '@paljs/ui/Button';
import { InputGroup } from '@paljs/ui/Input';
import { Checkbox } from '@paljs/ui/Checkbox';
import React from 'react';
import styled from 'styled-components';
import Link from 'next/link';

import Auth from 'components/Auth';
import Layout from 'Layouts';
import { AppDispatch } from 'redux/store';
import { connect } from 'react-redux';
import { register } from 'redux/slices/userSlice';
import { Spinner } from '@paljs/ui';
import { withRouter } from 'next/router';

const Input = styled(InputGroup)`
  margin-bottom: 2rem;
`;
class Register extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      email: '',
      password: '',
      passwordConfirm: '',
    };
  }

  handleChange = (e: any) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleRegister = (e: any) => {
    e.preventDefault();
    this.props.register(this.state.email, this.state.password);
    this.props.router.push('/auth/account-confirm');
  };

  onCheckbox = () => {
    // v will be true or false
  };
  render() {
    const { email, password, passwordConfirm } = this.state;
    return (
      <Layout title="Register">
        <Auth title="Create new account">
          <form action="" onSubmit={this.handleRegister}>
            <Input fullWidth>
              <input type="email" placeholder="Email Address" name="email" onChange={this.handleChange} />
            </Input>
            <Input fullWidth>
              <input type="password" placeholder="Password" name="password" onChange={this.handleChange} />
            </Input>
            <Input fullWidth>
              <input
                type="password"
                placeholder="Confirm Password"
                name="passwordConfirm"
                onChange={this.handleChange}
              />
            </Input>
            <Checkbox checked onChange={this.onCheckbox}>
              Agree to{' '}
              <Link href="/">
                <a>Terms & Conditions</a>
              </Link>
            </Checkbox>
            <Button
              status="Success"
              type="submit"
              shape="SemiRound"
              fullWidth
              onClick={this.handleRegister}
              style={{ position: 'relative' }}
              disabled={!email || !password || !passwordConfirm || this.props.registerLoading}
            >
              Register
              {this.props.registerLoading && <Spinner status="Primary" />}
            </Button>
          </form>
          <p>
            Already have an account?{' '}
            <Link href="/auth/login">
              <a>Log In</a>
            </Link>
          </p>
        </Auth>
      </Layout>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    authenticated: !state.user.access,
    registerLoading: state.user.registerLoading,
  };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
  return {
    register: (email: string, password: string) => dispatch(register(email, password)),
  };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Register));
