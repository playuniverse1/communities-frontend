import React, { Component } from 'react';
import { Card, CardBody } from '@paljs/ui/Card';
import { Button } from '@paljs/ui/Button';
import styled from 'styled-components';
import Slider from '@mui/material/Slider';

import Layout from 'Layouts';
import { Col, EvaIcon, InputGroup, Row, Spinner } from '@paljs/ui';
import { connect } from 'react-redux';
import { createRequest, fetchConfig } from 'redux/slices/userSlice';
import { AppDispatch } from 'redux/store';

const Input = styled(InputGroup)`
    margin-bottom: 10px;
`;

const LandingStyle = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 2rem;
    small {
        margin-bottom: 3rem;
    }
    h1 {
        margin-bottom: 0.5rem;
    }
    a {
        max-width: 20rem;
    }
    .bgImage {
        opacity: 0.5;
        width: 10em;
        position: absolute;
        bottom: 0;
        right: 0;
        margin-right: 1em;
        margin-bottom: 1em;
    }
    .bgImageLeft {
        left: 0;
        margin-left: 1em;
    }
     {
        .landingIcon {
            font-size: 4em;
        }
        ol {
            margin-top: 30px;
            list-style-type: none;
            display: flex;
            justify-content: center;
        }
        li {
            margin-right: 1em;
        }
        .listIcon {
            font-size: 3em;
            margin-top: 0.1em;
        }
    }
`;
export default function Landing(): JSX.Element {
    return (
        <Layout title="playUniverse communities">
            <Card>
                <CardBody>
                    <LandingStyle>
                        <h1>playUniverse communities</h1>
                        <small>Dein Community Projekt auf kostenlosen Servern.</small>
                        <img className="bgImage" src={'/images/keys.png'} />
                        <EvaIcon name="arrow-down" className="landingIcon" />
                    </LandingStyle>
                </CardBody>
            </Card>
            <Card>
                <CardBody>
                    <LandingStyle>
                        <h1>Und so funktionierts</h1>
                        {/* <img className='bgImage bgImageLeft' src={"/images/hearts.png"} /> */}
                        <ol>
                            <li>
                                <p>
                                    Reiche dein Community Projekt bei <strong>playUniverse</strong> ein
                                </p>{' '}
                                <EvaIcon name="email-outline" className="listIcon" />
                            </li>
                            <li>
                                <p>Teile uns mit welche Kapazität deine Community benötigt</p>
                                <EvaIcon name="bar-chart" className="listIcon" />
                            </li>
                            <li>
                                <p>Wir beurteilen deine Anfrage und geben dir dann innerhalb von 1 Tag bescheid</p>
                                <EvaIcon name="clock-outline" className="listIcon" />
                            </li>
                            <li>
                                <p>Den ganzen Prozess kannst du hier tranzparent verfolgen</p>
                                <EvaIcon name="eye-outline" className="listIcon" />
                            </li>
                            <li>
                                <p>
                                    Bei einem angenommenen Projekt erhältst du Zugriff auf das Fennek Panel um deine
                                    Ressourcen zu steuern
                                </p>
                                <EvaIcon name="monitor-outline" className="listIcon" />
                            </li>
                        </ol>
                        {/* <EvaIcon name="arrow-down" className='landingIcon'/> */}
                    </LandingStyle>
                </CardBody>
            </Card>
            <CardConnect />
        </Layout>
    );
}

class RequestCard extends Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            slots: 25,
            mem: 1,
            step: 0,
            firstName: '',
            lastName: '',
            email: '',
            communityName: '',
            homepage: '',
            description: '',
            password: '',
        };
    }

    componentDidMount() {
        this.props.fetchConfig();
        if (this.props.user.access) {
            this.setState({
                firstName: this.props.user.firstName,
                lastName: this.props.user.lastName,
                email: this.props.user.email,
            });
        }
    }

    handleChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    };

    navigatorNextDisabled = () => {
        const step = this.state.step;
        if (step == 1) {
            if (!this.state.communityName || !this.state.homepage || !this.state.description) {
                return true;
            }
        }
        if (step == 2) {
            if (!this.state.firstName || !this.state.lastName || !this.state.email || !this.state.password) {
                return true;
            }
        }
        return false;
    };

    render() {
        const { user } = this.props;
        const { minSlots, maxSlots, minMem, maxMem, formEnabled } = user.config;
        const state = this.state;
        return (
            <Card>
                <CardBody>
                    <LandingStyle>
                        <h1>
                            {formEnabled ? 'Jetzt kostenlose Server anfragen' : 'Aktuell ist das Portal geschlossen'}
                        </h1>
                        {/* <img className='bgImage bgImageLeft' src={"/images/hearts.png"} /> */}
                        {!formEnabled && (
                            <Row>
                                <Col>
                                    <p>Schau doch in ein paar Tagen nochmal vorbei.</p>
                                </Col>
                            </Row>
                        )}
                        <Row>
                            {this.state.step == 0 && (
                                <>
                                    <Col breakPoint={{ xs: 12, md: 6 }}>
                                        <Card>
                                            <header>Slots</header>
                                            <CardBody>
                                                <p>Wie viele Spieler sollen den Server parallel beitreten?</p>
                                                <Slider
                                                    defaultValue={minSlots}
                                                    max={maxSlots}
                                                    min={minSlots}
                                                    onChange={(x, e) => this.setState({ slots: e, x })}
                                                />
                                                <p>{this.state.slots} Spieler</p>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    <Col breakPoint={{ xs: 12, md: 6 }}>
                                        <Card>
                                            <header>RAM</header>
                                            <CardBody>
                                                <p>Wie viel RAM brauchst du? </p>
                                                <Slider
                                                    defaultValue={minMem}
                                                    max={Math.round(maxMem / 1000)}
                                                    min={Math.round(minMem / 1000)}
                                                    onChange={(x, e) => this.setState({ mem: e, x })}
                                                />
                                                <p>{this.state.mem} GB RAM</p>
                                            </CardBody>
                                        </Card>
                                    </Col>
                                    {this.props.configLoading && <Spinner />}
                                </>
                            )}
                            {this.state.step == 1 && (
                                <Col>
                                    <Card>
                                        <header>Was macht deine Community?</header>
                                        <CardBody>
                                            <Input fullWidth size="Large">
                                                <input
                                                    type="text"
                                                    placeholder="Name"
                                                    name="communityName"
                                                    onChange={this.handleChange}
                                                />
                                            </Input>
                                            <Input fullWidth size="Large">
                                                <input
                                                    type="text"
                                                    placeholder="Website"
                                                    name="homepage"
                                                    onChange={this.handleChange}
                                                />
                                            </Input>
                                            <Input fullWidth size="Large">
                                                <textarea
                                                    placeholder="Stellt euch kurz vor?"
                                                    name="description"
                                                    onChange={this.handleChange}
                                                ></textarea>
                                            </Input>
                                        </CardBody>
                                    </Card>
                                </Col>
                            )}
                            {this.state.step == 2 && (
                                <Col>
                                    <Card>
                                        <header>Damit wir DICH kontaktieren können</header>
                                        <CardBody>
                                            <Input fullWidth size="Large">
                                                <input
                                                    type="text"
                                                    placeholder="Vorname"
                                                    name="firstName"
                                                    defaultValue={this.props.user.firstName}
                                                    onChange={this.handleChange}
                                                />
                                            </Input>
                                            <Input fullWidth size="Large">
                                                <input
                                                    type="text"
                                                    placeholder="Nachname"
                                                    name="lastName"
                                                    defaultValue={this.props.user.lastName}
                                                    onChange={this.handleChange}
                                                />
                                            </Input>
                                            <Input fullWidth size="Large">
                                                <input
                                                    type="email"
                                                    defaultValue={this.props.user.email}
                                                    name="email"
                                                    placeholder="E-Mail"
                                                    onChange={this.handleChange}
                                                />
                                            </Input>
                                            {!this.props.user.access && (
                                                <Input fullWidth size="Large">
                                                    <input
                                                        type="password"
                                                        placeholder="Passwort"
                                                        name="password"
                                                        onChange={this.handleChange}
                                                    />
                                                </Input>
                                            )}
                                        </CardBody>
                                    </Card>
                                </Col>
                            )}
                            {this.state.step == 3 && (
                                <Col>
                                    <Card>
                                        <header>Bitte checke dein Postfach</header>
                                        <CardBody>
                                            <p>
                                                Damit du den Prozess nachverfolgen kannst haben wir dir ein Konto
                                                erstellt.
                                            </p>
                                        </CardBody>
                                    </Card>
                                </Col>
                            )}
                        </Row>
                        {this.state.step < 2 && (
                            <Row>
                                <Col>
                                    {this.state.step > 0 && (
                                        <Button
                                            style={{ marginRight: '1em' }}
                                            onClick={() => this.setState({ step: this.state.step - 1 })}
                                        >
                                            Zurück
                                        </Button>
                                    )}
                                    <Button
                                        onClick={() => this.setState({ step: this.state.step + 1 })}
                                        disabled={this.navigatorNextDisabled()}
                                    >
                                        Weiter
                                    </Button>
                                </Col>
                            </Row>
                        )}
                        {this.state.step == 2 && (
                            <Row>
                                <Col>
                                    <Button
                                        onClick={() => {
                                            this.props.createRequest(
                                                state.slots,
                                                state.mem,
                                                state.firstName,
                                                state.lastName,
                                                state.email,
                                                state.password,
                                                state.communityName,
                                                state.homepage,
                                                state.description,
                                            );
                                            this.setState({ step: 3 });
                                        }}
                                        disabled={this.navigatorNextDisabled()}
                                    >
                                        Weiter
                                    </Button>
                                </Col>
                            </Row>
                        )}
                    </LandingStyle>
                </CardBody>
            </Card>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        authenticated: !state.user.access,
        user: state.user,
        configLoading: state.user.fetchConfigLoading,
    };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        fetchConfig: () => dispatch(fetchConfig()),
        createRequest: (
            slots: any,
            mem: any,
            firstName: any,
            lastName: any,
            email: any,
            password: string,
            communityName: any,
            homepage: any,
            description: any,
        ) =>
            dispatch(
                createRequest(slots, mem, firstName, lastName, email, password, communityName, homepage, description),
            ),
    };
};

export const CardConnect = connect(mapStateToProps, mapDispatchToProps)(RequestCard);
