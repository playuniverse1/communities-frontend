import { Card, CardBody } from '@paljs/ui/Card';
import { InputGroup } from '@paljs/ui/Input';
import Col from '@paljs/ui/Col';
import Row from '@paljs/ui/Row';
import React from 'react';
import styled from 'styled-components';
import Layout from 'Layouts';
import { Button, Checkbox, Spinner } from '@paljs/ui';
import { AppDispatch } from 'redux/store';
import { connect } from 'react-redux';
import { withRouter } from 'next/router';
import { fetchConfig, fetchUser, updateConfig } from 'redux/slices/userSlice';

const Input = styled(InputGroup)`
    margin-bottom: 10px;
`;

class ConfigPage extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        const config = this.props.user.config;
        this.state = {
            minMem: config.minMem,
            maxMem: config.maxMem,
            minSlots: config.minSlots,
            maxSlots: config.maxSlots,
            formEnabled: config.formEnabled,
        };
    }

    handleChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    };

    componentDidMount() {
        this.props.fetchConfig();
        if (!this.props.user.isStaff) {
            this.props.router.push('/');
        }
    }

    render() {
        const { minMem, maxMem, minSlots, maxSlots, formEnabled } = this.state;
        const { firstName, config } = this.props.user;
        const { configLoading } = this.props;
        return (
            <Layout title="Profile">
                <Row>
                    <Col>
                        <Card>
                            <header>Hi {firstName}</header>
                            <CardBody>
                                <span>
                                    Du bist Admin! Hier kannst du allgemeine Funktionen zur Plattform verwalten.
                                </span>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row>
                    <Col breakPoint={{ xs: 12, md: 8 }}>
                        <Card>
                            <header>Anfrage Formular</header>
                            <CardBody>
                                <Input fullWidth size="Large">
                                    <p>Minimum RAM</p>
                                    <input
                                        type="number"
                                        placeholder="minMem"
                                        defaultValue={config.minMem}
                                        name="minMem"
                                        onChange={this.handleChange}
                                    />
                                </Input>
                                <Input fullWidth size="Large">
                                    <p>Maximum RAM</p>
                                    <input
                                        type="number"
                                        placeholder="maxMem"
                                        defaultValue={config.maxMem}
                                        name="maxMem"
                                        onChange={this.handleChange}
                                    />
                                </Input>
                                <Input fullWidth size="Large">
                                    <p>Minimum Slots</p>
                                    <input
                                        type="number"
                                        placeholder="minSlots"
                                        defaultValue={config.minSlots}
                                        name="minSlots"
                                        onChange={this.handleChange}
                                    />
                                </Input>
                                <Input fullWidth size="Large">
                                    <p>Maximum Slots</p>
                                    <input
                                        type="number"
                                        placeholder="maxSlots"
                                        defaultValue={config.maxMem}
                                        name="maxSlots"
                                        onChange={this.handleChange}
                                    />
                                </Input>
                                <Input>
                                    <p style={{ marginRight: '1em' }}>Formular aktiv</p>
                                    <Checkbox
                                        onChange={() => this.setState({ formEnabled: !formEnabled })}
                                        checked={formEnabled}
                                    />
                                </Input>

                                <Button
                                    style={{ marginTop: '1.5em', marginRight: '1em' }}
                                    disabled={configLoading}
                                    onClick={() =>
                                        this.props.updateConfig(minMem, maxMem, minSlots, maxSlots, formEnabled)
                                    }
                                >
                                    Speichern {this.props.updateProfileLoading && <Spinner status="Primary" />}
                                </Button>
                                <div style={{ height: '10em' }}></div>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Layout>
        );
    }
}
const mapStateToProps = (state: any) => {
    return {
        authenticated: !state.user.access,
        user: state.user,
        firstName: state.user.firstName,
        lastName: state.user.lastName,
        email: state.user.email,
        configLoading: state.user.fetchConfigLoading,
    };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        fetchUser: () => dispatch(fetchUser()),
        fetchConfig: () => dispatch(fetchConfig()),
        updateConfig: (minMem: number, maxMem: number, minSlots: number, maxSlots: number, formEnabled: boolean) =>
            dispatch(updateConfig(minMem, maxMem, minSlots, maxSlots, formEnabled)),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ConfigPage));
