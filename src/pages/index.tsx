import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import { useSelector } from 'react-redux';
import { getUser } from 'redux/slices/userSlice';

export default function Index() {
    const router = useRouter();
    const user = useSelector(getUser);
    useEffect(() => {
        localStorage.getItem('jwt');
        if (localStorage.getItem('jwt') && user.isStaff) {
            router.push('/admin');
        } else {
            router.push('/landing');
        }
    }),
        [];
    return <div />;
}
