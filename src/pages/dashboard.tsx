import React from 'react';
import Layout from 'Layouts';
import Row from '@paljs/ui/Row';
import { ButtonLink, Card, CardBody, Col, Select } from '@paljs/ui';
import styled from 'styled-components';

import { connect } from 'react-redux';
import Router, { withRouter } from 'next/router';
import { AppDispatch } from 'redux/store';
import { fetchUserRequests, getCommunity, Request } from 'redux/slices/userSlice';
import { RequestCard } from './admin';

export const SelectStyled = styled(Select)`
    margin-bottom: 1rem;
    width: 10em;
`;

const HomeStyle = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 2rem;
    small {
        margin-bottom: 3rem;
    }
    h1 {
        margin-bottom: 0.5rem;
    }
    a {
        max-width: 20rem;
    }
    header {
        font-weight: 600;
        font-size: 1.2em;
    }
`;

class Home extends React.Component<any, any> {
    intervalId: any;

    constructor(props: any) {
        super(props);
    }

    componentDidUpdate() {
        if (!this.props.user.access && !(typeof window === undefined)) {
            Router.push('/auth/login');
        }
    }

    componentWillUnmount() {}

    componentDidMount() {
        this.props.fetchRequests();
        if (this.props.user.isStaff) {
            this.props.router.push('/admin');
        }
    }

    render() {
        const { userRequests, communities } = this.props;
        return (
            <Layout title="Home">
                <Card>
                    <CardBody>
                        <HomeStyle>
                            <h1>
                                {userRequests ? `Du hast ${userRequests.length} Anfragen` : 'Keine Anfragen gefunden'}
                            </h1>
                            {!userRequests && (
                                <>
                                    <small>Du hast aktuell keine Anfragen.</small>
                                    <ButtonLink
                                        fullWidth
                                        appearance="hero"
                                        shape="Rectangle"
                                        onClick={() => Router.push('/landing')}
                                    >
                                        Neue Anfrage stellen
                                    </ButtonLink>
                                </>
                            )}
                        </HomeStyle>
                    </CardBody>
                </Card>
                {communities &&
                    userRequests.map(
                        (request: Request) =>
                            request &&
                            getCommunity(communities, request.community) && (
                                <RequestCard
                                    request={request}
                                    community={getCommunity(communities, request.community)}
                                />
                            ),
                    )}

                <Row>
                    <Col breakPoint={{ xs: 12, md: 6 }}>
                        <Card>
                            <header>Wie funktioniert das eigentlich?</header>
                            <CardBody>
                                <ul>
                                    <li>Du reichst dein Projekt ein</li>
                                    <li>Wir prüfen deine Anfrage und geben dir in ~2 Tagen bescheid</li>
                                    <li>Du erhältst Zugriff auf das Kontrollsystem</li>
                                </ul>
                                <Row />
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Layout>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        user: state.user,
        userRequests: state.user.userRequests,
        userRequestsLoading: state.userRequestsLoading,
        communities: state.user.communities,
    };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        fetchRequests: () => dispatch(fetchUserRequests()),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Home));
