import { Card, CardBody } from '@paljs/ui/Card';
import { InputGroup } from '@paljs/ui/Input';
import Col from '@paljs/ui/Col';
import Row from '@paljs/ui/Row';
import React from 'react';
import styled from 'styled-components';
import Layout from 'Layouts';
import { Button, Spinner } from '@paljs/ui';
import { AppDispatch } from 'redux/store';
import { connect } from 'react-redux';
import Link from 'next/link';
import { withRouter } from 'next/router';
import { deleteUser, fetchUser, updateProfile } from 'redux/slices/userSlice';

export const currentRegion = [
    { label: 'Not selected' },
    { label: 'US-West (Oregon)', value: 'us-west-2' },
    { label: 'Canada Central', value: 'ca-central-1' },
];

const Input = styled(InputGroup)`
    margin-bottom: 10px;
`;

class ProfilePage extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            firstName: null,
            lastName: null,
        };
    }

    handleChange = (e: any) => {
        this.setState({ [e.target.name]: e.target.value });
    };

    handleProfileUpdate = () => {
        this.props.updateProfile(this.state.firstName, this.state.lastName);
    };

    handleDeleteProfile = () => {
        this.props.deleteUser();
        this.props.router.push('/auth/logout');
    };

    componentDidMount() {
        this.props.fetchUser();
        this.setState({ firstName: this.props.user.firstName, lastName: this.props.user.lastName });
    }

    render() {
        const { firstName, lastName, email } = this.props.user;
        const { updateProfileLoading } = this.props;
        return (
            <Layout title="Profile">
                <Row>
                    <Col breakPoint={{ xs: 12, md: 8 }}>
                        <Card>
                            <header>Dein Account bei playUniverse</header>
                            <CardBody>
                                <Input fullWidth size="Large">
                                    <input
                                        type="text"
                                        placeholder="Vorname"
                                        defaultValue={firstName}
                                        name="firstName"
                                        onChange={this.handleChange}
                                    />
                                </Input>
                                <Input fullWidth size="Large">
                                    <input
                                        type="text"
                                        placeholder="Nachname"
                                        defaultValue={lastName}
                                        name="lastName"
                                        onChange={this.handleChange}
                                    />
                                </Input>
                                <Input fullWidth size="Large">
                                    <input type="text" disabled placeholder="E-Mail" defaultValue={email} />
                                </Input>
                                <Button
                                    style={{ marginTop: '1.5em', marginRight: '1em' }}
                                    onClick={this.handleProfileUpdate}
                                    disabled={updateProfileLoading}
                                >
                                    Speichern {this.props.updateProfileLoading && <Spinner status="Primary" />}
                                </Button>
                                <Button
                                    status="Danger"
                                    style={{ marginTop: '1.5em' }}
                                    onClick={this.handleDeleteProfile}
                                    disabled={updateProfileLoading}
                                >
                                    Löschen {this.props.deleteProfileLoading && <Spinner status="Primary" />}
                                </Button>
                                <div style={{ height: '10em' }}></div>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col breakPoint={{ xs: 12, md: 4 }}>
                        <Card>
                            <header>Wir schuetzen deine Daten!</header>
                            <CardBody>
                                <p>Deine Anfragen- und Profildaten liegen auf sicheren Servern in Deutschland</p>
                                <p>Weitere Informationen</p>
                                <Link href={'/privacy'}>Hier</Link>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </Layout>
        );
    }
}
const mapStateToProps = (state: any) => {
    return {
        authenticated: !state.user.access,
        user: state.user,
        firstName: state.user.firstName,
        lastName: state.user.lastName,
        email: state.user.email,
        updateProfileLoading: state.user.updateProfileLoading,
    };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        fetchUser: () => dispatch(fetchUser()),
        updateProfile: (firstName: string, lastName: string) => dispatch(updateProfile(firstName, lastName)),
        deleteUser: () => dispatch(deleteUser()),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(ProfilePage));
