import React from 'react';
import Layout from 'Layouts';
import Row from '@paljs/ui/Row';
import { Button, ButtonLink, Card, CardBody, CardHeader, Col, Select, Spinner } from '@paljs/ui';
import styled from 'styled-components';

import { connect, useSelector } from 'react-redux';
import Router, { withRouter } from 'next/router';
import { AppDispatch } from 'redux/store';
import {
    changeRequestStatus,
    Community,
    deleteRequest,
    Egg,
    fetchEggs,
    fetchRequests,
    getCommunity,
    getUser,
    Request,
} from 'redux/slices/userSlice';

export const SelectStyled = styled(Select)`
    margin-bottom: 1rem;
    width: 10em;
`;

const parseToSelect = (list: Egg[], label: string, value: string) => {
    let x: { label: string; value: string }[] = [];
    list.map((e) => {
        x.push({
            label: (e as any)[label],
            value: (e as any)[value],
        });
    });
    return x;
};

const HomeStyle = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    align-items: center;
    margin-bottom: 2rem;
    small {
        margin-bottom: 3rem;
    }
    h1 {
        margin-bottom: 0.5rem;
    }
    a {
        max-width: 20rem;
    }
    header {
        font-weight: 600;
        font-size: 1.2em;
    }
`;

interface RequestCardProps {
    request: Request;
    community: Community;
    handleChange?: Function;
    changeLoading?: boolean;
    eggs?: Egg[];
    handleInstallationChange?: Function;
    installation?: string;
    deleteRequest?: Function;
}

const REQUEST_STATUS = [
    {
        id: 0,
        label: 'Offen',
        color: 'Primary',
    },
    {
        id: 1,
        label: 'In Review',
        color: 'Warning',
    },
    {
        id: 2,
        label: 'Warten auf Antwort',
        color: 'Warning',
    },
    {
        id: 3,
        label: 'Angenommen',
        color: 'Success',
    },
    {
        id: 4,
        label: 'Abgelehnt',
        color: 'Danger',
    },
    {
        id: 5,
        label: 'Beendet',
        color: 'Secondary',
    },
    {
        id: 6,
        label: 'Abgebrochen',
        color: 'Secondary',
    },
];

export const RequestCard = ({
    request,
    community,
    handleChange,
    changeLoading,
    eggs,
    handleInstallationChange,
    installation,
    deleteRequest,
}: RequestCardProps) => {
    const { label } = REQUEST_STATUS[request.status];
    const user = useSelector(getUser);
    if (
        handleChange != null &&
        changeLoading != null &&
        eggs != null &&
        handleInstallationChange != null &&
        installation != null &&
        deleteRequest != null
    ) {
        return (
            <Card accent="Primary">
                <CardHeader>
                    <span style={{ fontSize: '1.8em' }}>{label}</span>{' '}
                    <span style={{ fontSize: '1.5em' }}>
                        | {request.queue > 0 ? 'Platz: ' + request.queue : ''} {community.name}
                    </span>
                </CardHeader>
                <CardBody>
                    <Row>
                        <Col breakPoint={{ xs: 12, md: 4 }}>
                            <strong>Community</strong>

                            <p>
                                <strong>Name: </strong> {community.name}
                            </p>
                            <p>
                                <strong>Homepage: </strong> {community.homepage}
                            </p>
                            <p>
                                <strong>E-Mail: </strong> {community.email}
                            </p>
                            {community.discord && (
                                <p>
                                    <strong>Dsicord: </strong> {community.discord}
                                </p>
                            )}
                            <p>
                                <strong>Beschreibung: </strong> {community.description}
                            </p>
                        </Col>
                        <Col breakPoint={{ xs: 12, md: 4 }}>
                            <strong>Konfiguration</strong>
                            <p>
                                <strong>RAM: </strong> <code>{request.memory}</code>
                            </p>
                            <p>
                                <strong>Slots: </strong> <code>{request.slots}</code>
                            </p>
                        </Col>
                        {user.isStaff ? (
                            <Col breakPoint={{ xs: 12, md: 4 }}>
                                <strong>Aktionen</strong>
                                <Row style={{ marginTop: '1em' }}>
                                    <Col>
                                        {request.status == 0 && (
                                            <Button
                                                status="Primary"
                                                disabled={changeLoading}
                                                onClick={() => handleChange(request.id, 1)}
                                            >
                                                Zuweisen
                                            </Button>
                                        )}
                                    </Col>
                                    <Col style={{ marginTop: '0.5em' }}>
                                        {request.status == 1 && (
                                            <>
                                                <Button
                                                    status="Success"
                                                    style={{ marginRight: '0.5em' }}
                                                    disabled={changeLoading}
                                                    onClick={() => handleChange(request.id, 3)}
                                                >
                                                    Annehmen{' '}
                                                </Button>
                                                <Button
                                                    status="Warning"
                                                    disabled={changeLoading}
                                                    onClick={() => handleChange(request.id, 4)}
                                                >
                                                    Zurückweisen{' '}
                                                </Button>
                                            </>
                                        )}
                                        {request.status == 3 && request.serverId == 0 && (
                                            <div>
                                                <p>Minecraft Installation:</p>
                                                <Select
                                                    options={parseToSelect(eggs, 'name', 'uuid')}
                                                    size="Small"
                                                    onChange={(e) => handleInstallationChange(e)}
                                                />
                                                <Button
                                                    status="Primary"
                                                    style={{ marginTop: '2em' }}
                                                    onClick={() => handleChange(request.id, 3)}
                                                    disabled={changeLoading || !installation}
                                                >
                                                    Server anlegen
                                                </Button>
                                            </div>
                                        )}
                                        {request.status == 3 && request.serverId > 0 && (
                                            <div>
                                                <strong>Server</strong>
                                                <p>
                                                    <strong>IP und Port: </strong>{' '}
                                                    <code>{request.serverIp + ':' + request.serverPort}</code>
                                                </p>
                                                <p>
                                                    <a
                                                        href={
                                                            'https://cp.playuniverse.eu/server/' +
                                                            request.serverFriendlyId
                                                        }
                                                        target="_blank"
                                                    >
                                                        Fennek Panel
                                                    </a>
                                                </p>
                                            </div>
                                        )}
                                    </Col>
                                    <Col style={{ marginTop: '0.5em' }}>
                                        <Button
                                            status="Danger"
                                            disabled={changeLoading}
                                            onClick={() => deleteRequest(request.id)}
                                        >
                                            Löschen{' '}
                                        </Button>
                                    </Col>
                                    {changeLoading && <Spinner />}
                                </Row>
                            </Col>
                        ) : (
                            <Col breakPoint={{ xs: 12, md: 4 }}>
                                {request.status == 3 && request.serverId > 0 && (
                                    <div>
                                        <strong>Server</strong>
                                        <p>
                                            <strong>IP und Port: </strong>{' '}
                                            <code>{request.serverIp + ':' + request.serverPort}</code>
                                        </p>
                                        <p>
                                            <a
                                                href={'https://cp.playuniverse.eu/server/' + request.serverFriendlyId}
                                                target="_blank"
                                            >
                                                Fennek Panel
                                            </a>
                                        </p>
                                    </div>
                                )}
                            </Col>
                        )}
                    </Row>
                </CardBody>
            </Card>
        );
    } else {
        return (
            <Card accent="Primary">
                <CardHeader>
                    <span style={{ fontSize: '1.8em' }}>{label}</span>{' '}
                    <span style={{ fontSize: '1.5em' }}>
                        | {request.queue > 0 ? 'Platz: ' + request.queue : ''} {community.name}
                    </span>
                </CardHeader>
                <CardBody>
                    <Row>
                        <Col breakPoint={{ xs: 12, md: 4 }}>
                            <strong>Community</strong>

                            <p>
                                <strong>Name: </strong> {community.name}
                            </p>
                            <p>
                                <strong>Homepage: </strong> {community.homepage}
                            </p>
                            <p>
                                <strong>E-Mail: </strong> {community.email}
                            </p>
                            {community.discord && (
                                <p>
                                    <strong>Dsicord: </strong> {community.discord}
                                </p>
                            )}
                            <p>
                                <strong>Beschreibung: </strong> {community.description}
                            </p>
                        </Col>
                        <Col breakPoint={{ xs: 12, md: 4 }}>
                            <strong>Konfiguration</strong>
                            <p>
                                <strong>RAM: </strong> <code>{request.memory}</code>
                            </p>
                            <p>
                                <strong>Slots: </strong> <code>{request.slots}</code>
                            </p>
                        </Col>
                    </Row>
                </CardBody>
            </Card>
        );
    }
};

class Admin extends React.Component<any, any> {
    constructor(props: any) {
        super(props);
        this.state = {
            installation: '',
        };
    }

    componentDidMount() {
        this.props.fetchRequests();
        this.props.fetchEggs();
    }

    componentDidUpdate() {
        if (!this.props.user.access && !this.props.user.isStaff && !(typeof window === undefined)) {
            Router.push('/auth/login');
        }
    }

    countOpenRequests = (requests: Request[]) => {
        let openRequests = 0;
        requests.map((request) => {
            if (request.status == 0 || request.status == 1) {
                openRequests++;
            }
        });
        return openRequests;
    };

    render() {
        const { adminRequests, communities, firstName, adminRequestsLoading, requestChangeLoading } = this.props.user;
        return (
            <Layout title="Admin">
                {adminRequests.length ? (
                    <>
                        <Card>
                            <CardBody>
                                <HomeStyle>
                                    <h1>Hallo {firstName}</h1>
                                    <small>
                                        Du hast <strong>{this.countOpenRequests(adminRequests)}</strong> offene Anfragen
                                    </small>
                                    <ButtonLink appearance="hero" shape="Rectangle" onClick={this.props.fetchRequests}>
                                        Neuladen
                                        {adminRequestsLoading && <Spinner />}
                                    </ButtonLink>
                                </HomeStyle>
                            </CardBody>
                        </Card>
                        {adminRequests.map(
                            (request: Request) =>
                                request &&
                                getCommunity(communities, request.community) && (
                                    <RequestCard
                                        request={request}
                                        community={getCommunity(communities, request.community)}
                                        handleChange={(e: any, x: any) =>
                                            this.props.changeStatus(e, x, this.state.installation)
                                        }
                                        changeLoading={requestChangeLoading}
                                        eggs={this.props.user.eggs}
                                        handleInstallationChange={(e: any) => this.setState({ installation: e.value })}
                                        installation={this.state.installation}
                                        deleteRequest={this.props.deleteRequest}
                                    />
                                ),
                        )}
                    </>
                ) : (
                    <Card>
                        <CardBody>
                            <HomeStyle>
                                <h1>Keine Anfragen gefunden</h1>
                                <small>Du bist auf dem neusten Stand.</small>
                                <ButtonLink appearance="hero" shape="Rectangle" onClick={this.props.fetchRequests}>
                                    Neuladen
                                </ButtonLink>
                            </HomeStyle>
                        </CardBody>
                    </Card>
                )}
            </Layout>
        );
    }
}

const mapStateToProps = (state: any) => {
    return {
        user: state.user,
    };
};

const mapDispatchToProps = (dispatch: AppDispatch) => {
    return {
        fetchRequests: () => dispatch(fetchRequests()),
        fetchEggs: () => dispatch(fetchEggs()),
        changeStatus: (request: string, status: number, installation?: string) =>
            dispatch(changeRequestStatus(request, status, installation)),
        deleteRequest: (request: string) => dispatch(deleteRequest(request)),
    };
};

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Admin));
