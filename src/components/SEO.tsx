import React from 'react';
import Head from 'next/head';

const SEO: React.FC<SEOProps> = ({ description, keywords, title }) => (
    <Head>
        <title>{title} | playUniverse Communities</title>
        <meta name="description" content={description} />
        <meta name="keywords" content={keywords?.join(', ')} />
        <meta property="og:type" content="website" />
        <meta name="og:title" property="og:title" content={title} />
        <meta name="og:description" property="og:description" content={description} />
        <meta property="og:site_name" content="" />
        <meta property="og:url" content="" />
        <meta name="twitter:card" content="summary" />
        <meta name="twitter:title" content={title} />
        <meta name="twitter:description" content={description} />
        <meta name="twitter:site" content="" />
        <meta name="twitter:creator" content="@AhmedElywh" />
        <meta name="twitter:image" content="" />
        <meta property="og:image" content="" />
        <link rel="icon" type="image/png" href="/icons/favicon-32x32.png" />
        <link rel="apple-touch-icon" type="image/png" href="/icons/favicon-32x32.png" />
    </Head>
);

export interface SEOProps {
    description?: string;
    lang?: string;
    meta?: any[];
    keywords?: string[];
    title: string;
}

SEO.defaultProps = {
    description: 'playUniverse Communitiess helps you setting up cloud gaming in a really simple way',
    keywords: [
        'cloud-gaming',
        'aws',
        'playUniverse Communities',
        'gaming-cloud',
        'cloudgaming',
        'shadow',
        'free-game-panel',
        'gaming',
    ],
};

export default SEO;
